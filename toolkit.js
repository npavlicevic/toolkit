var WebSockets = function(url) {
  // work with sockets
  // FOR TEST use this url here ---> "ws://localhost/ws"
  var root = this;

  root.conn = new WebSocket(url);
};
WebSockets.prototype.send = function(message) {
  this.conn.send(JSON.stringify(message));
};
WebSockets.prototype.sendText = function(message) {
  this.conn.send(message);
};
WebSockets.prototype.raw = function() {
  return this.conn;
};
// WebSockets
var Collection = function(items) {
  var root = this;

  root.collection = [];
  root.length = 0;
  root.init(items);
};
Collection.prototype.push = function(item) {
  this.collection.push(item);
  this.length = this.collection.length
};
Collection.prototype.fetchLeft = function() {
  if(this.length > 0) return this.collection[0];
};
Collection.prototype.popLeft = function() {
  if(this.length <= 0) return;
  this.collection.shift();
  this.length = this.collection.length;
};
Collection.prototype.fetchRight = function() {
  if(this.length > 0) return this.collection[this.length - 1];
};
Collection.prototype.popRight = function() {
  if(this.length <= 0) return;
  this.collection.pop();
  this.length = this.collection.length;
};
Collection.prototype.clone = function(anotherCollection) {
  this.collection.forEach(function(item) {
    anotherCollection.push(item);
  });
};
Collection.prototype.init = function(items) {
  if(items === undefined) return;
  items.forEach(function(item) {
    this.push(item);
  }.bind(this));
};
// Collection
var Map = function(pairs) {
  var root = this;

  root.map = {};
  root.init(pairs);
};
Map.prototype.insert = function(key, value) {
  this.map[key] = value;
};
Map.prototype.value = function(key) {
  return this.map[key];
};
Map.prototype.remove = function(key) {
  delete this.map[key];
};
Map.prototype.init = function(pairs) {
  if(pairs === undefined) return;
  for(key in pairs) this.insert(key, pairs[key])
};
// Map
var Element = function(element, type) {
  var root = this;

  root.element = element;
  root.dom = undefined;
  root.libWrap = undefined;
  root.type = type;
};
Element.prototype.select = function(alib) {
  this.dom = document.querySelector(this.element);
  this.libWrap = alib(this.dom);
  return this;
};
Element.prototype.raw = function() {
  return this.dom;
};
Element.prototype.lib = function() {
  return this.libWrap;
};
Element.prototype.rect = function() {
  return this.dom.getBoundingClientRect();
};
Element.prototype.top = function() {
  return this.rect().top;
};
Element.prototype.left = function() {
  return this.rect().left;
};
Element.prototype.bottom = function() {
  return this.rect().bottom;
};
Element.prototype.right = function() {
  return this.rect().right;
};
// Element
var Canvas = function(container, element) {
  var root = this;

  root.container = container;
  root.element = element;
  root.dom = undefined;
  root.libWrap = undefined;
  root.context = undefined;
};
Canvas.prototype.select = function(alib) {
  this.container.select(alib);
  this.dom = document.querySelector(this.element);
  this.dom.setAttribute("width", this.container.raw().clientWidth);
  this.dom.setAttribute("height", this.container.raw().clientHeight);
  this.libWrap = alib(this.dom);
  this.context = this.dom.getContext("2d");
  return this;
};
Canvas.prototype.raw = function() {
  return this.dom;
};
Canvas.prototype.lib = function() {
  return this.libWrap;
};
Canvas.prototype.beginPath = function(x, y) {
  this.context.beginPath();
  this.context.strokeStyle = "rgb(0, 0, 0)";
  this.context.lineWidth = 5;
  this.context.lineJoin = this.context.lineCap = "round";
  this.context.moveTo(x, y);
};
Canvas.prototype.stroke = function(x, y) {
  this.context.lineTo(x, y);
  this.context.stroke();
};
Canvas.prototype.dataUrl = function() {
  return this.dom.toDataURL();
};
Canvas.prototype.top = function() {
  return this.container.top();
};
Canvas.prototype.left = function() {
  return this.container.left();
};
Canvas.prototype.bottom = function() {
  return this.container.bottom();
};
Canvas.prototype.right = function() {
  return this.container.right();
};
// Canvas
var CanvasTheState = function() {
  var root = this;

  this.isDrawing = false;
};
// CanvasTheState
var HandlebarsView = function(element, map) {
  var root = this;

  root.element = element;
  root.map = map;
};
HandlebarsView.prototype.renderSimple = function(lib, map) {
  this.element.select(lib);
  if(map !== undefined) this.map = map.map;
  var compiled = Handlebars.compile(this.element.lib().html());
  return compiled(this.map);
  // todo this.map.map do this with get
};
// HandlebarsView
